exports.config = {
  tests: 'tests/*_test.js',
  output: './output',
  helpers: {
    WebDriver: {
      url: 'http://test.netex24.net',
      browser: 'chrome',
	  restart: false,
      windowSize: '1920x1680'
    }
  },
  include: {
    I: './steps_file.js'
  },
  bootstrap: null,
  mocha: {},
  name: 'repo'
}