
Feature('Qiwi btc');

Scenario('Create transaction', (I) => {
 I.amOnPage('http://test.netex24.net/#/ru/');
 I.waitForElement('.target-direction', 20)
 I.click('#source-direction-32'); //выбираем киви в исходной валюте
 I.click('#target-direction-134'); //выбираем биткоин в результрирующей валюте
 I.waitForText('Обмен Qiwi', 20, '.calculator-wrapper'); //ждем загрузки формы
 I.fillField('.v-input-input', '1'); //заполняем форму
 I.pressKey('Tab');
 I.pressKey('79233492772');
 I.wait('1');
 I.pressKey('Tab');
 I.pressKey('Tab');
 I.pressKey('2MvjoYGwrutKJhgunG1PHvioLePo6Rp4WzT ');
 I.wait('2');
 I.pressKey('Tab');
 I.pressKey('79233492772');
 I.pressKey('Tab');
 I.pressKey('sembat@inbox.ru');
 I.wait('2');
 I.click('Перейти к оплате');
 I.waitForElement('.vTransactionTimer', 20); //ждем загрузку страницы обмена с таймером
 I.see('Оплатите заявку до окончания этого времени', '.vTransactionTimer'); //смотрим, что страница загрузилась
 I.click('Оплатить');
 I.switchToNextTab();
 I.waitForText('Перевод на QIWI Кошелек', 25, '.content-self-75');
 I.see('Перевод на QIWI Кошелек', '.content-self-75');
});

Scenario('Confirm tranasction', (I) => {
 I.amOnPage('https://netex-admin-ui-test.azurewebsites.net');
 I.waitForElement('#loginForm', 20)
 I.fillField('username', 'test');
 I.fillField('password', 'test');
 I.click('Войти');
 I.waitForText('Обмены', 20, '.sidebar-nav');
 I.click('Обмены', '.sidebar-nav');
 I.waitForText('Исходная валюта:', '4', '.form-group');
 I.click('#filterSourceCurrency');
 I.waitForText('Qiwi RUB Wallet', 5, '#filterSourceCurrency');
 I.wait('2');
 I.click('//option[@label="Qiwi RUB Wallet"]', '#filterSourceCurrency'); //выбираем Qiwi RUB Wallet
 I.click('//button[@ng-click="applyFilter()"]'); //нажимаем кнопку применить
 I.wait('4');
 I.dontSee('.preloader-cell');
 I.click('.i-1'); //кликаем ссылку на детали в последнем обмене
 I.waitForText('Зачислить средства', '20', '.transaction-control');
 I.click('Зачислить средства');
 I.waitForText('Комментарий:', 20, '.comment');
 I.fillField('comment', 'test comment'); //вводим комментарий
 I.click('//button[@ng-click="yes()"]'); //нажимаем кнопку Да
 I.waitForText('Отправить перевод', 25, '.transaction-control');
 I.click('Отправить перевод');
 I.waitForText('Отправка перевода', 20, '.modal-header');
 I.click('//button[@ng-click="sendTransfer()"]'); //подтверждаем отправку
 I.waitForText('Результат:', 30, '.modal-body');
 I.click('//button[@ng-click="dismiss()"]');
 I.waitForText('Обрабатывать автоматически', 20, '.transaction-control');
 I.click('Обрабатывать автоматически', '.transaction-control');
 I.click('//button[@ng-click="yes()"]'); //нажимаем кнопку Да
 I.waitForText('Перевод денег начат', 20, '#transactionStatuses');
 I.click('транзакция на витрине', '.modal-body');
 I.wait('2');
 I.switchToNextTab();
 I.waitForText('Обмен завершен', 360, '.vCompleted'); //ждем завершения обмена
 I.see('Обмен завершен', '.vCompleted');
 I.switchToPreviousTab();
 I.click('//button[@ng-click="addToBlackList()"]'); //кликаем кнопку Добавить в черный список, чтобы обновить данные транзакции
 I.click('//button[@ng-click="discard()"]'); //закрываем окно черного списка
 I.waitForText('Хеш транзакции Bitcoin:', 50, '.modal-body');
 let hash = await I.grabTextFrom('');
 I.wait('1');
 });
/*
Scenario('Check Bitflow', (I) => {
 I.amOnPage('https://bitflow-ui-test.azurewebsites.net/#/');
 I.waitForElement('#f-appId', 20)
 I.fillField('#f-appId', 'cc1c6f29-5e19-4cc9-ba7b-c6859e4b9d6b');
 I.fillField('#f-appSecret', '9fTCWng1kKiX1DzeOU9FOGCn19UJgDpWR5RtNmuJtc4=');
 I.click('Вход');
 I.click('Транзакции', '.vHeader');
 I.waitForText('Транзакции', 200, '.adresses-table');
 I.fillField('#__BVID__88', hash); //вводим хеш транзакции в поиск
 I.see('0.00000148', '.t-address'); 
 I.see('2MvjoYGwrutKJhgunG1PHvioLePo6Rp4WzT', '.t-address'); 
 });
*/